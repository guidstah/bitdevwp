## What is this? ##
This is a tweaked cross-platform Wordpress development example environment created by Guido Baars for BIT-students based on scotchbox, vagrant and virtualbox.

To start open (git)terminal and run:
'vagrant up'

Before you can do this some setup is required... See below for more info


## Installation ##
Note: if you are using Windows, download and install git and use the git-terminal for commands.

1. Get virtualbox at www.virtualbox.org
2. Get vagrant at www.vagrantup.com
3. Clone this repo to your development folder (I like to create seperate folder for this)
4. Edit the 'Vagrantfile' for customization (like adding a theme folder)
4. Open terminal. Run 'vagrant up', wait until its done, it will fail a bit...
5. Run 'vagrant ssh' (this will get you an ssh session into the vagrant vm), then 'cd /var/www/public/', then 'wp core download'
6. Run 'logout' to leave the vm ssh session, when back on host machine run 'vagrant provision'. This will force provisioning, wich in this case means installing and configuring wordpress. For details check the provisioning scripts which are pretty basic.
7. Last but not least, I like to add a domain name to my vm by adding a line to /etc/hosts file: '192.168.33.10 bit.dev' where bit.dev is the domain name of choice.
8. Happy development, your LAMP stack can be reached on 192.168.33.10 or your configured domain in the webbrowser.

## How do I use this? ##
~ Starting ~
From now on run 'vagrant up' in the dev folder to start the development vm.

~ Database ~
Connect with program like MySQL Workbench to manually save and restore database dumps for various projects.
Details:
Connection method:		Standard TCP/IP over SSH
SSH Hostname:			127.0.0.1:2222
SSH Username:			vagrant
SSH Password:			vagrant
MySQL Hostname:			127.0.0.1
MySQL Server port:		3306
Username:				root
Password:				root

~ Destroy or suspend ~
Destroy means removing the vm with virtualdisk and all database data inside
Suspend means suspending the vm while keeping database data intact
commands:
'vagrant destroy'
'vagrant suspend'

~ Folders ~
'public' folder contains wordpress, plugins etc. this gets synced with the vm lamp folder
'tools' contains some basic provision scripts
'db' this is where I store my database dumps, but you can use any place you like

~ Add theme for theme development ~
Create a folder outside the development environment folder where you store your theme files, add the entry to the Vagrantfile and run 'vagrant reload' or 'vagrant destroy && vagrant up' to apply changes. The folder becomes synced with the vm theme folder if applied correctly.

~ ToDo ~
Find a method to save and restore database dumps with a provision script. Because lazy is kinda cool...
Idea: import/export dumps with provisioned ssh script that will log into mysql cmd line interface to achieve dump import/export. 

## Can I use some other (web) project instead of Wordpress? ##
Sure, but this one is tweaked for Wordpress. The regular scotch box is advised (see github). Scotchbox is a single vagrant box, but there are many more and different ones and you can even create your own for any kind of project. For simple provisioning examples you can check out this tweaked version.