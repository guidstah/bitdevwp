#!/bin/bash
# Script om snel Wordpress te installeren

# Configuratie
wp core config --path=/var/www/public --dbname=scotchbox --dbuser=root --dbpass=root --dbprefix=wp_ --debug

echo "Setup.sh - Wp is geconfigureerd"

# Installatie
wp core install --user=vagrant --title=BitDev --admin_user=root --admin_password=root --admin_email=admin@localhost.dev --path=/var/www/public --debug --url=bit.dev

echo "Setup.sh - Wp is geinstalleerd"