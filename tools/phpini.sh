#!/bin/bash

# Optimize php settings for wordpress
echo "max_execution_time = 600" >> /etc/php5/apache2/conf.d/user.ini
echo "upload_max_filesize = \"50M\"" >> /etc/php5/apache2/conf.d/user.ini

echo "phpini.sh - Php tweaks uitgevoerd!"
